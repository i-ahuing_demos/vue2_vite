import Vue from 'vue'
import VueRouter from 'vue-router'

import Detail from '../views/Detail.vue'
import Form from '../views/Form.vue'
import Home from '../views/Home.vue'
import List from '../views/List.vue'
import Store from '../views/Store.vue'
import Store2 from '../views/Store2.vue'

Vue.use(VueRouter)
let router = new VueRouter({
  base: '/',
  mode: 'history',
  routes: [
    {
      component: Home,
      name: 'Home',
      path: '/',
    },
    {
      component: List,
      name: 'List',
      path: '/list',
    },
    {
      component: Detail,
      name: 'Detail',
      path: '/detail',
    },
    {
      component: Form,
      name: 'Form',
      path: '/form',
    },
    {
      component: Store,
      name: 'Store',
      path: '/store',
    },
    {
      component: Store2,
      name: 'Store2',
      path: '/store2',
    },
  ],
})

export default router
