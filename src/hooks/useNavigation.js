import { useRoute, useRouter, } from 'vue-router/composables'
export default function() {
  const router = useRouter()
  const route = useRoute()
  router.push = new Proxy(router.push, {
    apply(
      target, thisArg, args
    ) {
      const removeTab = args[0].removeTab
      // const name = route.name
      const result = target.apply(thisArg, args)?.then((res) => {
        if (removeTab) {
          // store.commit('removeTab', { name, })
        }
        return res
      })
      return result
    },
  })
  return {
    query: route.query ?? {},
    route,
    router,
  }
}
