// https://eslint.org/docs/user-guide/configuring

module.exports = {
  env: {
    browser: true,
    node: true,
  },
  extends: [
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/recommended',
    'plugin:react/recommended',
    './.eslintrc-auto-import.json',
    // 'standard',
  ],
  globals: {
    BMAP_NORMAL_MAP: true,
    BMAP_SATELLITE_MAP: true,
    MOBILE_PREFIX: true,
    __DEV__: true,
    // vue
    defineRender: true,
    mainReload: true,
    modules: true,
    platform: true,
    platformApi: true,
    platformTag: true,
    routerBase: true,
    userTag: true,
  },
  parserOptions: {
    ecmaFeatures: { jsx: true, },
    parser: '@babel/eslint-parser',
  },
  plugins: [
    'vue',
    'react',
    'import-newlines',
    'simple-import-sort',
    'sort-keys-vue-fix',
  ],
  root: true,
  // add your custom rules here
  rules: {
    // 'object-property-newline': 'error',

    // arrray 数组超过3个item 括号换行
    'array-bracket-newline': [
      'error',
      {
        minItems: 3,
        multiline: true,
      },
    ],
    'array-bracket-spacing': [ 'error', 'always', ],
    'array-callback-return': 'error',
    // 数组超过3个item item换行
    'array-element-newline': [
      'error',
      {
        minItems: 3,
        multiline: true,
      },
    ],
    'arrow-spacing': 'error',
    'block-spacing': 1,
    'brace-style': 1,
    camelcase: [ 'error', { properties: 'never', }, ],
    'comma-dangle': [
      'error',
      {
        arrays: 'always',
        exports: 'never',
        functions: 'ignore',
        imports: 'always',
        objects: 'always',
      },
    ], //对象字面量项尾不能有逗号
    'comma-spacing': [
      'error',
      {
        after: true,
        before: false,
      },
    ],
    'comma-style': [ 'error', 'last', ],
    complexity: 1,

    // 'no-extra-parens': 1, //禁止不必要的括号
    // 'require-atomic-updates': 1,
    curly: 1,
    'dot-location': 1,
    'dot-notation': 1,
    eqeqeq: 0,
    'func-call-spacing': [ 'error', 'never', ],
    'function-paren-newline': [ 'error', { minItems: 3, }, ],
    // "vue/component-name-in-template-casing": ["error", "PascalCase", { registeredComponentsOnly: true }],
    // don't require .vue extension when importing
    /*'import/extensions': ['error', 'always', {
          js: 'never',
          vue: 'never'
        }],*/
    // disallow reassignment of function parameters
    // disallow parameter object manipulation except for specific exclusions
    /*'no-param-reassign': ['error', {
          props: true,
          ignorePropertyModificationsFor: [
            'state', // for vuex state
            'acc', // for reduce accumulators
            'e' // for e.returnvalue
          ]
        }],*/
    // allow async-await
    'generator-star-spacing': 'off',
    'implicit-arrow-linebreak': [ 'error', 'beside', ],
    'import-newlines/enforce': [
      'error',
      {
        forceSingleLine: false,
        // 需要与 object-curly-newline 一致
        items: 3,
        semi: false,
      },
    ],
    indent: [ 2, 2, ], //缩进风格
    'jsx-quotes': [ 'error', 'prefer-single', ],
    'key-spacing': [
      'error',
      {
        afterColon: true,
        beforeColon: false,
      },
    ],
    'keyword-spacing': [
      'error',
      {
        after: true,
        before: true,
      },
    ],
    'max-len': [
      'error',
      {
        code: 120,
        ignoreComments: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreTrailingComments: true,
        ignoreUrls: true,
      },
    ],
    'max-nested-callbacks': [ 'error', 4, ],
    // 多个三目
    'multiline-ternary': [ 'error', 'always-multiline', ],
    'no-await-in-loop': 1,
    'no-cond-assign': 1,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-div-regex': 1,
    'no-dupe-args': 1,
    'no-dupe-keys': 'error',
    'no-duplicate-imports': [ 'error', { includeExports: true, }, ],
    'no-else-return': 1,
    'no-empty': 1,
    'no-empty-function': 1,
    'no-empty-pattern': 1,
    'no-eq-null': 1,
    'no-eval': 1,
    'no-extra-bind': 1,
    'no-extra-boolean-cast': 1,
    'no-extra-label': 1,
    // 'no-extra-parens': 1, //禁止不必要的括号
    'no-extra-semi': 1, //禁止不必要的分号
    'no-fallthrough': 1,
    'no-global-assign': 1,

    'no-implicit-globals': 1,
    'no-inner-declarations': 1, //禁止不必要的分号
    'no-invalid-this': 1,
    'no-irregular-whitespace': 1, //禁止不规则的空白
    'no-labels': 1,
    'no-lone-blocks': 1,
    'no-lonely-if': 'error',
    'no-loop-func': 1,
    'no-magic-numbers': 0,
    'no-mixed-operators': 'error',
    'no-mixed-spaces-and-tabs': 'error',
    'no-multi-spaces': 1, //不能用多余的空格
    // 最大空行
    'no-multiple-empty-lines': [
      'error',
      {
        max: 1,
        maxBOF: 1,
      },
    ],
    'no-new': 1,
    'no-new-wrappers': 1,
    'no-octal': 1,
    'no-prototype-builtins': 0, // hasOwnProperty 1- 禁止直接使用 Object.prototypes 的内置属性
    'no-redeclare': 1,
    'no-regex-spaces': 1,
    'no-restricted-properties': 1,
    'no-return-assign': 1,
    'no-self-assign': 1,
    'no-self-compare': 1,
    'no-sequences': 1,
    'no-spaced-func': 'error',
    'no-sparse-arrays': 1,
    'no-template-curly-in-string': 1,
    'no-undef': 1,
    'no-undef-init': 1,
    'no-unexpected-multiline': 1,
    'no-unmodified-loop-condition': 1,
    'no-unreachable': 1,
    'no-unsafe-negation': 1,
    'no-unused-expressions': 1,
    // 'no-useless-catch': 1,
    'no-unused-labels': 1,
    'no-unused-vars': 1,
    'no-use-before-define': 1,
    'no-useless-escape': 1,
    'no-useless-return': 1,
    'no-var': 'error',
    'no-warning-comments': 1,
    // 'no-nested-ternary': "error",
    'no-whitespace-before-property': 'error',
    'no-with': 1,
    // object
    'object-curly-newline': [
      'error',
      {
        ExportDeclaration: {
          minProperties: 3,
          multiline: true,
        },
        ImportDeclaration: {
          minProperties: 3,
          multiline: true,
        },
        // multiline: true,
        // minProperties: 3,
        // "ObjectExpression": "never",
        ObjectExpression: {
          minProperties: 3,
          multiline: true,
        },
        ObjectPattern: {
          minProperties: 3,
          multiline: true,
        },
      },
    ],

    'object-curly-spacing': [ 'error', 'always', ],

    'object-property-newline': [ 'error', { allowAllPropertiesOnSameLine: false, }, ],
    'operator-linebreak': [ 'error', 'after', ],
    'padded-blocks': [ 'error', 'never', ],

    'prefer-promise-reject-errors': 1,
    'prefer-template': 'error',
    'quote-props': [ 'error', 'as-needed', ],
    quotes: [ 'error', 'single', ],
    'react/display-name': 0,
    // tag-aligned有问题
    'react/jsx-closing-bracket-location': [ 1, 'line-aligned', ],
    'react/jsx-closing-tag-location': 1,
    'react/jsx-curly-brace-presence': [
      'error',
      {
        children: 'never',
        props: 'never',
      },
    ],
    'react/jsx-first-prop-new-line': [ 'error', 'multiline', ],
    'react/jsx-indent-props': [ 2, 2, ], //验证JSX中的props缩进
    // 限制JSX中单行上的props的最大数量
    'react/jsx-max-props-per-line': [ 1, { maximum: 1, }, ],
    // a target _blank 打开会加上rel="noreferrer" 消除安全隐患
    'react/jsx-no-target-blank': 0,
    'react/jsx-no-undef': 0,
    // 'react/jsx-one-expression-per-line': [ 1, { allow: 'none', }, ],
    // 'react/jsx-no-leaked-render': [ 1, { validStrategies: [ 'ternary', ], }, ],
    'react/jsx-props-no-multi-spaces': 1,
    'react/jsx-sort-props': [
      1,
      {
        callbacksLast: true,
        // react 保留关键字靠前
        reservedFirst: true,
      },
    ],
    //
    'react/jsx-wrap-multilines': [
      1,
      {
        arrow: 'parens-new-line',
        assignment: 'parens-new-line',
        condition: 'parens-new-line',
        declaration: 'parens-new-line',
        logical: 'parens-new-line',
        prop: 'parens-new-line',
        return: 'parens-new-line',
      },
    ],
    'react/no-string-refs': 0,
    'react/no-unknown-property': 0,

    'react/prop-types': 0,
    // 'react/jsx-newline': [ 1, { prevent: true, }, ],
    'react/react-in-jsx-scope': 0,
    'react/self-closing-comp': 1,
    'require-await': 1,
    semi: [ 'error', 'never', ],
    'simple-import-sort/exports': 'error',
    'simple-import-sort/imports': 'error',
    'sort-keys-vue-fix/sort-keys-vue-fix': 'warn',
    'space-before-blocks': 'error',
    'space-before-function-paren': [ 'error', 'never', ],
    'space-in-parens': [ 'error', 'never', ],
    'space-infix-ops': 'error',
    'spaced-comment': 0, // 注释内有空格或者缩进
    'template-curly-spacing': 'error',
    'use-isnan': 1,
    // 'require-atomic-updates': 1,
    'valid-typeof': 1,
    'vars-on-top': 1,
    'vue/attribute-hyphenation': [
      'error',
      'always',
      { ignore: [], },
    ],
    'vue/component-definition-name-casing': [ 'error', 'PascalCase', ],
    'vue/custom-event-name-casing': [
      'error',
      'camelCase',
      { ignores: [], },
    ],
    'vue/html-indent': [
      'error',
      2,
      {
        alignAttributesVertically: true,
        attribute: 1,
        baseIndent: 1,
        closeBracket: 0,
        ignores: [],
      },
    ],
    'vue/key-spacing': [
      0,
      {
        afterColon: true,
        beforeColon: false,
      },
    ], //对象字面量中冒号的前后空格
    'vue/multi-word-component-names': 0,
    'vue/mustache-interpolation-spacing': [ 'error', 'always', ],
    'vue/no-arrow-functions-in-watch': 1,
    'vue/no-computed-properties-in-data': 1,
    'vue/no-deprecated-data-object-declaration': 1,
    'vue/no-multi-spaces': [ 'error', { ignoreProperties: false, }, ],
    'vue/no-mutating-props': 1,
    'vue/no-reserved-component-names': 0,
    'vue/no-side-effects-in-computed-properties': 1,
    'vue/no-spaces-around-equal-signs-in-attribute': [ 'error', ],
    'vue/no-unused-components': 1,
    'vue/prop-name-casing': [ 'error', 'camelCase', ],
    'vue/require-direct-export': 1,
    'vue/require-name-property': 1,
    'vue/require-prop-type-constructor': 1,
    'vue/require-render-return': 1,
    'vue/require-valid-default-prop': 1,

    'vue/return-in-computed-property': 1,
    'wrap-iife': 1,
  },
}
