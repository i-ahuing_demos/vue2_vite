const presets = [
  // 根、子目录的公共预设
  [
    '@babel/preset-env',
    {
      modules: false,
      targets: {
        browsers: [
          '> 1%',
          'last 2 versions',
          'not ie <= 8',
        ],
      },
    },
  ],
  // 手动关闭，不然setup中不能使用jsx
  // [ '@vue/cli-plugin-babel/preset', { jsx: false, }, ],
  [
    '@vue/babel-preset-jsx',
    {
      compositionAPI: true,
      functional: false,
      vModel: true,
    },
  ],
]
const plugins = [
  // 根、子目录的公共插件
  '@babel/plugin-transform-runtime',
]

module.exports = {
  babelrcRoots: [ '.', 'packages/*', ],
  plugins,
  presets,
}
