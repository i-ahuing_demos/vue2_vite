import { fileURLToPath, URL, } from 'node:url'

import AutoImort from 'unplugin-auto-import/vite'
import AutoImortVueComponent from 'unplugin-vue-components/vite'
import VueMacros from 'unplugin-vue-macros/vite'
import { defineConfig, } from 'vite'
import { createHtmlPlugin, } from 'vite-plugin-html'

import config from './vue-macros.config'
// https://vitejs.dev/config/
export default defineConfig({
  esbuild: { jsx: 'preserve', },
  plugins: [
    VueMacros(config),
    // 自动导入
    AutoImort({
      dirs: [ './src/hooks/*.js', ],
      eslintrc: {
        enabled: true,
        filepath: './.eslintrc-auto-import.json',
      },
      exclude: [
        /[\\/]node_modules(?!\/test)[\\/]/,
        /[\\/]\.git[\\/]/,
        /[\\/]\.nuxt[\\/]/,
      ],
      imports: [
        'vue',
        'vue-router',
        'pinia',
      ],
      include: [
        /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
        /\.vue$/,
        /\.vue\?vue/, // .vue
      ],
      vueTemplate: true,
    }),
    AutoImortVueComponent({
      allowOverrides: true,
      directives: false,
      dirs: [ './src/components/', './src/components2/', ],
      dts: false,
      extensions: [ 'vue', ],
      include: [
        /\.vue$/,
        /\.vue\?vue/,
        /\.js$/,
      ],
      version: 2.7,
    }),
    createHtmlPlugin({
      /**
       * After writing entry here, you will not need to add script tags in `index.html`, the original tags need to be deleted
       * @default src/main.ts
       */
      entry: '../src/main.js',
      filename: 'home.html',
      filename1: './html/index.html',
      minify: false,
      /**
       * If you want to store `index.html` in the specified folder, you can modify it, otherwise no configuration is required
       * @default index.html
       */
      template: 'public/index.html',
    }),
  ],
  resolve: { alias: { '@': fileURLToPath(new URL('./src', import.meta.url)), }, },
})
